# -*- coding: utf-8 -*-

#    ds18b20_dts
#    Copyright (C) 2018  Alecks Gates
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
import asyncio
from datetime import datetime
from io import StringIO
import math
from unittest.mock import MagicMock, Mock
import unittest.mock

from domaintypesystem import DomainTypeSystem
import pytest

from temperature_relay_bridge import temperature_relay_bridge


class AsyncMock(MagicMock):
    async def __call__(self, *args, **kwargs):
        return super(AsyncMock, self).__call__(*args, **kwargs)


@pytest.mark.asyncio
async def test_handle_temperature_old_timestamp(monkeypatch):
    mock_serial_number = b'foo'
    mock_last_temperature_event = temperature_relay_bridge.TemperatureEvent(
        temperature=b'',
        timestamp=900000,
        model=b'',
        serial_number=mock_serial_number)
    mock_latest_temperature_events = {mock_serial_number: mock_last_temperature_event}
    monkeypatch.setattr(temperature_relay_bridge, 'latest_temperature_events', mock_latest_temperature_events)
    mock_last_received_time = 795
    mock_last_received_time_nanoseconds = mock_last_received_time * 1e9
    mock_temp_state = {"last_received_time": mock_last_received_time}
    monkeypatch.setattr(temperature_relay_bridge, 'temp', mock_temp_state)
    mock_state = 789
    mock_gpio_state = {"state": mock_state}
    monkeypatch.setattr(temperature_relay_bridge, 'gpio', mock_gpio_state)

    mock_timestamp = 1337
    mock_temperature_event = temperature_relay_bridge.TemperatureEvent(
        temperature=b'',
        timestamp=mock_timestamp,
        model=b'',
        serial_number=mock_serial_number)

    return_value = await temperature_relay_bridge.handle_temperature(mock_temperature_event,
                                                                     None,
                                                                     mock_last_received_time_nanoseconds)

    assert return_value is None
    assert mock_latest_temperature_events[mock_serial_number].timestamp != mock_temperature_event.timestamp
    assert mock_temp_state["last_received_time"] == mock_last_received_time
    assert mock_gpio_state["state"] == mock_state


@pytest.mark.asyncio
async def test_handle_temperature_new_serial_number(monkeypatch):
    mock_serial_number = b'foo'
    mock_latest_temperature_events = {}
    monkeypatch.setattr(temperature_relay_bridge, 'latest_temperature_events', mock_latest_temperature_events)
    mock_gpio_state = {"state": None}
    monkeypatch.setattr(temperature_relay_bridge, 'gpio', mock_gpio_state)
    mock_temp_state = {}
    monkeypatch.setattr(temperature_relay_bridge, 'temp', mock_temp_state)

    mock_temperature = b'10.1'
    mock_timestamp = 1337e9
    mock_temperature_event = temperature_relay_bridge.TemperatureEvent(
        temperature=mock_temperature,
        timestamp=mock_timestamp,
        model=b'',
        serial_number=mock_serial_number)
    mock_received_timestamp = 59
    mock_received_timestamp_nanoseconds = mock_received_timestamp * 1e9

    return_value = await temperature_relay_bridge.handle_temperature(mock_temperature_event,
                                                                     None,
                                                                     mock_received_timestamp_nanoseconds)

    assert return_value is None
    assert mock_latest_temperature_events[mock_serial_number].timestamp == mock_temperature_event.timestamp
    assert mock_temp_state['last_received_time'] == mock_received_timestamp


@pytest.mark.asyncio
async def test_handle_temperature_new_timestamp(monkeypatch):
    mock_serial_number = b'foo'
    mock_last_temperature_event = temperature_relay_bridge.TemperatureEvent(
        temperature=b'',
        timestamp=0,
        model=b'',
        serial_number=mock_serial_number)
    mock_latest_temperature_events = {mock_serial_number: mock_last_temperature_event}
    monkeypatch.setattr(temperature_relay_bridge, 'latest_temperature_events', mock_latest_temperature_events)
    mock_temp_state = {}
    monkeypatch.setattr(temperature_relay_bridge, 'temp', mock_temp_state)

    mock_temperature = b'10.1'
    mock_timestamp = 1337e9
    mock_temperature_event = temperature_relay_bridge.TemperatureEvent(
        temperature=mock_temperature,
        timestamp=mock_timestamp,
        model=b'',
        serial_number=mock_serial_number)
    mock_received_timestamp = 59
    mock_received_timestamp_nanoseconds = mock_received_timestamp * 1e9

    return_value = await temperature_relay_bridge.handle_temperature(mock_temperature_event,
                                                                     None,
                                                                     mock_received_timestamp_nanoseconds)

    assert return_value is None
    assert mock_latest_temperature_events[mock_serial_number].timestamp == mock_temperature_event.timestamp
    assert mock_temp_state['last_received_time'] == mock_received_timestamp


@pytest.mark.asyncio
async def test_handle_temperature_closed_open(monkeypatch):
    mock_serial_number = b'foo'
    mock_latest_temperature_events = {}
    monkeypatch.setattr(temperature_relay_bridge, 'latest_temperature_events', mock_latest_temperature_events)
    mock_last_received_time = 795
    mock_last_received_time_nanoseconds = mock_last_received_time * 1e9
    mock_temp_state = {"last_received_time": mock_last_received_time}
    monkeypatch.setattr(temperature_relay_bridge, 'temp', mock_temp_state)
    mock_relay_closed = 1
    monkeypatch.setattr(temperature_relay_bridge, 'RELAY_CLOSED', mock_relay_closed)
    mock_relay_open = 0
    monkeypatch.setattr(temperature_relay_bridge, 'RELAY_OPEN', mock_relay_open)
    mock_gpio_state = {"state": mock_relay_closed}
    monkeypatch.setattr(temperature_relay_bridge, 'gpio', mock_gpio_state)
    mock_target_temp = 15.0
    monkeypatch.setattr(temperature_relay_bridge, 'TARGET_TEMP', mock_target_temp)

    mock_timestamp = 1337
    mock_timestamp_nanoseconds = mock_timestamp * 1e9
    mock_temperature = b'30.0'
    mock_temperature_event = temperature_relay_bridge.TemperatureEvent(
        temperature=mock_temperature,
        timestamp=mock_timestamp_nanoseconds,
        model=b'',
        serial_number=mock_serial_number)

    assert mock_gpio_state["state"] == mock_relay_closed
    return_value = await temperature_relay_bridge.handle_temperature(mock_temperature_event,
                                                                     None,
                                                                     mock_last_received_time_nanoseconds)

    assert return_value is None
    assert mock_gpio_state["state"] == mock_relay_open


@pytest.mark.asyncio
async def test_handle_temperature_average_closed_open(monkeypatch):
    mock_serial_number_one = b'bar'
    mock_serial_number_two = b'foo'
    mock_last_temperature_event = temperature_relay_bridge.TemperatureEvent(
        temperature=b'5.0',
        timestamp=900e9,
        model=b'',
        serial_number=mock_serial_number_one)
    mock_latest_temperature_events = {mock_serial_number_one: mock_last_temperature_event}
    monkeypatch.setattr(temperature_relay_bridge, 'latest_temperature_events', mock_latest_temperature_events)
    mock_last_received_time = 795
    mock_last_received_time_nanoseconds = mock_last_received_time * 1e9
    mock_temp_state = {"last_received_time": mock_last_received_time}
    monkeypatch.setattr(temperature_relay_bridge, 'temp', mock_temp_state)
    mock_relay_closed = 1
    monkeypatch.setattr(temperature_relay_bridge, 'RELAY_CLOSED', mock_relay_closed)
    mock_relay_open = 0
    monkeypatch.setattr(temperature_relay_bridge, 'RELAY_OPEN', mock_relay_open)
    mock_gpio_state = {"state": mock_relay_closed}
    monkeypatch.setattr(temperature_relay_bridge, 'gpio', mock_gpio_state)
    mock_target_temp = 15.0
    monkeypatch.setattr(temperature_relay_bridge, 'TARGET_TEMP', mock_target_temp)

    mock_timestamp = 1337
    mock_timestamp_nanoseconds = mock_timestamp * 1e9
    mock_temperature = b'30.0'
    mock_temperature_event = temperature_relay_bridge.TemperatureEvent(
        temperature=mock_temperature,
        timestamp=mock_timestamp_nanoseconds,
        model=b'',
        serial_number=mock_serial_number_two)

    assert mock_gpio_state["state"] == mock_relay_closed
    return_value = await temperature_relay_bridge.handle_temperature(mock_temperature_event,
                                                                     None,
                                                                     mock_last_received_time_nanoseconds)

    assert return_value is None
    assert mock_gpio_state["state"] == mock_relay_open


@pytest.mark.asyncio
async def test_handle_temperature_closed_closed(monkeypatch):
    mock_serial_number = b'foo'
    mock_latest_temperature_events = {}
    monkeypatch.setattr(temperature_relay_bridge, 'latest_temperature_events', mock_latest_temperature_events)
    mock_last_received_time = 795
    mock_last_received_time_nanoseconds = mock_last_received_time * 1e9
    mock_temp_state = {"last_received_time": mock_last_received_time}
    monkeypatch.setattr(temperature_relay_bridge, 'temp', mock_temp_state)
    mock_relay_closed = 1
    monkeypatch.setattr(temperature_relay_bridge, 'RELAY_CLOSED', mock_relay_closed)
    mock_relay_open = 0
    monkeypatch.setattr(temperature_relay_bridge, 'RELAY_OPEN', mock_relay_open)
    mock_gpio_state = {"state": mock_relay_closed}
    monkeypatch.setattr(temperature_relay_bridge, 'gpio', mock_gpio_state)
    mock_target_temp = 15.0
    monkeypatch.setattr(temperature_relay_bridge, 'TARGET_TEMP', mock_target_temp)

    mock_timestamp = 1337
    mock_timestamp_nanoseconds = mock_timestamp * 1e9
    mock_temperature = b'14.0'
    mock_temperature_event = temperature_relay_bridge.TemperatureEvent(
        temperature=mock_temperature,
        timestamp=mock_timestamp_nanoseconds,
        model=b'',
        serial_number=mock_serial_number)

    assert mock_gpio_state["state"] == mock_relay_closed
    return_value = await temperature_relay_bridge.handle_temperature(mock_temperature_event,
                                                                     None,
                                                                     mock_last_received_time_nanoseconds)

    assert return_value is None
    assert mock_gpio_state["state"] == mock_relay_closed


@pytest.mark.asyncio
async def test_handle_temperature_average_closed_closed(monkeypatch):
    mock_serial_number_one = b'bar'
    mock_serial_number_two = b'foo'
    mock_last_temperature_event = temperature_relay_bridge.TemperatureEvent(
        temperature=b'-1.0',
        timestamp=900e9,
        model=b'',
        serial_number=mock_serial_number_one)
    mock_latest_temperature_events = {mock_serial_number_one: mock_last_temperature_event}
    monkeypatch.setattr(temperature_relay_bridge, 'latest_temperature_events', mock_latest_temperature_events)
    mock_last_received_time = 795
    mock_last_received_time_nanoseconds = mock_last_received_time * 1e9
    mock_temp_state = {"last_received_time": mock_last_received_time}
    monkeypatch.setattr(temperature_relay_bridge, 'temp', mock_temp_state)
    mock_relay_closed = 1
    monkeypatch.setattr(temperature_relay_bridge, 'RELAY_CLOSED', mock_relay_closed)
    mock_relay_open = 0
    monkeypatch.setattr(temperature_relay_bridge, 'RELAY_OPEN', mock_relay_open)
    mock_gpio_state = {"state": mock_relay_closed}
    monkeypatch.setattr(temperature_relay_bridge, 'gpio', mock_gpio_state)
    mock_target_temp = 15.0
    monkeypatch.setattr(temperature_relay_bridge, 'TARGET_TEMP', mock_target_temp)

    mock_timestamp = 1337
    mock_timestamp_nanoseconds = mock_timestamp * 1e9
    mock_temperature = b'30.0'
    mock_temperature_event = temperature_relay_bridge.TemperatureEvent(
        temperature=mock_temperature,
        timestamp=mock_timestamp_nanoseconds,
        model=b'',
        serial_number=mock_serial_number_two)

    assert mock_gpio_state["state"] == mock_relay_closed
    return_value = await temperature_relay_bridge.handle_temperature(mock_temperature_event,
                                                                     None,
                                                                     mock_last_received_time_nanoseconds)

    assert return_value is None
    assert mock_gpio_state["state"] == mock_relay_closed


@pytest.mark.asyncio
async def test_handle_temperature_open_closed(monkeypatch):
    mock_serial_number = b'foo'
    mock_latest_temperature_events = {}
    monkeypatch.setattr(temperature_relay_bridge, 'latest_temperature_events', mock_latest_temperature_events)
    mock_last_received_time = 795
    mock_last_received_time_nanoseconds = mock_last_received_time * 1e9
    mock_temp_state = {"last_received_time": mock_last_received_time}
    monkeypatch.setattr(temperature_relay_bridge, 'temp', mock_temp_state)
    mock_relay_closed = 1
    monkeypatch.setattr(temperature_relay_bridge, 'RELAY_CLOSED', mock_relay_closed)
    mock_relay_open = 0
    monkeypatch.setattr(temperature_relay_bridge, 'RELAY_OPEN', mock_relay_open)
    mock_gpio_state = {"state": mock_relay_open}
    monkeypatch.setattr(temperature_relay_bridge, 'gpio', mock_gpio_state)
    mock_target_temp = 15.0
    monkeypatch.setattr(temperature_relay_bridge, 'TARGET_TEMP', mock_target_temp)

    mock_timestamp = 1337
    mock_timestamp_nanoseconds = mock_timestamp * 1e9
    mock_temperature = b'13.0'
    mock_temperature_event = temperature_relay_bridge.TemperatureEvent(
        temperature=mock_temperature,
        timestamp=mock_timestamp_nanoseconds,
        model=b'',
        serial_number=mock_serial_number)

    assert mock_gpio_state["state"] == mock_relay_open
    return_value = await temperature_relay_bridge.handle_temperature(mock_temperature_event,
                                                                     None,
                                                                     mock_last_received_time_nanoseconds)

    assert return_value is None
    assert mock_gpio_state["state"] == mock_relay_closed


@pytest.mark.asyncio
async def test_handle_temperature_average_open_closed(monkeypatch):
    mock_serial_number_one = b'bar'
    mock_serial_number_two = b'foo'
    mock_last_temperature_event = temperature_relay_bridge.TemperatureEvent(
        temperature=b'30.0',
        timestamp=900e9,
        model=b'',
        serial_number=mock_serial_number_one)
    mock_latest_temperature_events = {mock_serial_number_one: mock_last_temperature_event}
    monkeypatch.setattr(temperature_relay_bridge, 'latest_temperature_events', mock_latest_temperature_events)
    mock_last_received_time = 795
    mock_last_received_time_nanoseconds = mock_last_received_time * 1e9
    mock_temp_state = {"last_received_time": mock_last_received_time}
    monkeypatch.setattr(temperature_relay_bridge, 'temp', mock_temp_state)
    mock_relay_closed = 1
    monkeypatch.setattr(temperature_relay_bridge, 'RELAY_CLOSED', mock_relay_closed)
    mock_relay_open = 0
    monkeypatch.setattr(temperature_relay_bridge, 'RELAY_OPEN', mock_relay_open)
    mock_gpio_state = {"state": mock_relay_open}
    monkeypatch.setattr(temperature_relay_bridge, 'gpio', mock_gpio_state)
    mock_target_temp = 15.0
    monkeypatch.setattr(temperature_relay_bridge, 'TARGET_TEMP', mock_target_temp)

    mock_timestamp = 1337
    mock_timestamp_nanoseconds = mock_timestamp * 1e9
    mock_temperature = b'-1.0'
    mock_temperature_event = temperature_relay_bridge.TemperatureEvent(
        temperature=mock_temperature,
        timestamp=mock_timestamp_nanoseconds,
        model=b'',
        serial_number=mock_serial_number_two)

    assert mock_gpio_state["state"] == mock_relay_open
    return_value = await temperature_relay_bridge.handle_temperature(mock_temperature_event,
                                                                     None,
                                                                     mock_last_received_time_nanoseconds)

    assert return_value is None
    assert mock_gpio_state["state"] == mock_relay_closed


@pytest.mark.asyncio
async def test_handle_temperature_open_open(monkeypatch):
    mock_serial_number = b'foo'
    mock_latest_temperature_events = {}
    monkeypatch.setattr(temperature_relay_bridge, 'latest_temperature_events', mock_latest_temperature_events)
    mock_last_received_time = 795
    mock_last_received_time_nanoseconds = mock_last_received_time * 1e9
    mock_temp_state = {"last_received_time": mock_last_received_time}
    monkeypatch.setattr(temperature_relay_bridge, 'temp', mock_temp_state)
    mock_relay_closed = 1
    monkeypatch.setattr(temperature_relay_bridge, 'RELAY_CLOSED', mock_relay_closed)
    mock_relay_open = 0
    monkeypatch.setattr(temperature_relay_bridge, 'RELAY_OPEN', mock_relay_open)
    mock_gpio_state = {"state": mock_relay_open}
    monkeypatch.setattr(temperature_relay_bridge, 'gpio', mock_gpio_state)
    mock_target_temp = 15.0
    monkeypatch.setattr(temperature_relay_bridge, 'TARGET_TEMP', mock_target_temp)

    mock_timestamp = 1337
    mock_timestamp_nanoseconds = mock_timestamp * 1e9
    mock_temperature = b'30.0'
    mock_temperature_event = temperature_relay_bridge.TemperatureEvent(
        temperature=mock_temperature,
        timestamp=mock_timestamp_nanoseconds,
        model=b'',
        serial_number=mock_serial_number)

    assert mock_gpio_state["state"] == mock_relay_open
    return_value = await temperature_relay_bridge.handle_temperature(mock_temperature_event,
                                                                     None,
                                                                     mock_last_received_time_nanoseconds)

    assert return_value is None
    assert mock_gpio_state["state"] == mock_relay_open


@pytest.mark.asyncio
async def test_handle_temperature_average_open_open(monkeypatch):
    mock_serial_number_one = b'bar'
    mock_serial_number_two = b'foo'
    mock_last_temperature_event = temperature_relay_bridge.TemperatureEvent(
        temperature=b'30.0',
        timestamp=900e9,
        model=b'',
        serial_number=mock_serial_number_one)
    mock_latest_temperature_events = {mock_serial_number_one: mock_last_temperature_event}
    monkeypatch.setattr(temperature_relay_bridge, 'latest_temperature_events', mock_latest_temperature_events)
    mock_last_received_time = 795
    mock_last_received_time_nanoseconds = mock_last_received_time * 1e9
    mock_temp_state = {"last_received_time": mock_last_received_time}
    monkeypatch.setattr(temperature_relay_bridge, 'temp', mock_temp_state)
    mock_relay_closed = 1
    monkeypatch.setattr(temperature_relay_bridge, 'RELAY_CLOSED', mock_relay_closed)
    mock_relay_open = 0
    monkeypatch.setattr(temperature_relay_bridge, 'RELAY_OPEN', mock_relay_open)
    mock_gpio_state = {"state": mock_relay_open}
    monkeypatch.setattr(temperature_relay_bridge, 'gpio', mock_gpio_state)
    mock_target_temp = 15.0
    monkeypatch.setattr(temperature_relay_bridge, 'TARGET_TEMP', mock_target_temp)

    mock_timestamp = 1337
    mock_timestamp_nanoseconds = mock_timestamp * 1e9
    mock_temperature = b'1.0'
    mock_temperature_event = temperature_relay_bridge.TemperatureEvent(
        temperature=mock_temperature,
        timestamp=mock_timestamp_nanoseconds,
        model=b'',
        serial_number=mock_serial_number_two)

    assert mock_gpio_state["state"] == mock_relay_open
    return_value = await temperature_relay_bridge.handle_temperature(mock_temperature_event,
                                                                     None,
                                                                     mock_last_received_time_nanoseconds)

    assert return_value is None
    assert mock_gpio_state["state"] == mock_relay_open


@pytest.mark.asyncio
async def test_handle_temperature_open_open_threshold(monkeypatch):
    mock_serial_number = b'foo'
    mock_latest_temperature_events = {}
    monkeypatch.setattr(temperature_relay_bridge, 'latest_temperature_events', mock_latest_temperature_events)
    mock_last_received_time = 795
    mock_last_received_time_nanoseconds = mock_last_received_time * 1e9
    mock_temp_state = {"last_received_time": mock_last_received_time}
    monkeypatch.setattr(temperature_relay_bridge, 'temp', mock_temp_state)
    mock_relay_closed = 1
    monkeypatch.setattr(temperature_relay_bridge, 'RELAY_CLOSED', mock_relay_closed)
    mock_relay_open = 0
    monkeypatch.setattr(temperature_relay_bridge, 'RELAY_OPEN', mock_relay_open)
    mock_gpio_state = {"state": mock_relay_open}
    monkeypatch.setattr(temperature_relay_bridge, 'gpio', mock_gpio_state)
    mock_target_temp = 15.0
    monkeypatch.setattr(temperature_relay_bridge, 'TARGET_TEMP', mock_target_temp)

    mock_timestamp = 1337
    mock_timestamp_nanoseconds = mock_timestamp * 1e9
    mock_temperature = b'14.8'
    mock_temperature_event = temperature_relay_bridge.TemperatureEvent(
        temperature=mock_temperature,
        timestamp=mock_timestamp_nanoseconds,
        model=b'',
        serial_number=mock_serial_number)

    assert mock_gpio_state["state"] == mock_relay_open
    return_value = await temperature_relay_bridge.handle_temperature(mock_temperature_event,
                                                                     None,
                                                                     mock_last_received_time_nanoseconds)

    assert return_value is None
    assert mock_gpio_state["state"] == mock_relay_open


@pytest.mark.asyncio
async def test_handle_temperature_average_open_open(monkeypatch):
    mock_serial_number_one = b'bar'
    mock_serial_number_two = b'foo'
    mock_last_temperature_event = temperature_relay_bridge.TemperatureEvent(
        temperature=b'14.9',
        timestamp=900e9,
        model=b'',
        serial_number=mock_serial_number_one)
    mock_latest_temperature_events = {mock_serial_number_one: mock_last_temperature_event}
    monkeypatch.setattr(temperature_relay_bridge, 'latest_temperature_events', mock_latest_temperature_events)
    mock_last_received_time = 795
    mock_last_received_time_nanoseconds = mock_last_received_time * 1e9
    mock_temp_state = {"last_received_time": mock_last_received_time}
    monkeypatch.setattr(temperature_relay_bridge, 'temp', mock_temp_state)
    mock_relay_closed = 1
    monkeypatch.setattr(temperature_relay_bridge, 'RELAY_CLOSED', mock_relay_closed)
    mock_relay_open = 0
    monkeypatch.setattr(temperature_relay_bridge, 'RELAY_OPEN', mock_relay_open)
    mock_gpio_state = {"state": mock_relay_open}
    monkeypatch.setattr(temperature_relay_bridge, 'gpio', mock_gpio_state)
    mock_target_temp = 15.0
    monkeypatch.setattr(temperature_relay_bridge, 'TARGET_TEMP', mock_target_temp)

    mock_timestamp = 1337
    mock_timestamp_nanoseconds = mock_timestamp * 1e9
    mock_temperature = b'14.6'
    mock_temperature_event = temperature_relay_bridge.TemperatureEvent(
        temperature=mock_temperature,
        timestamp=mock_timestamp_nanoseconds,
        model=b'',
        serial_number=mock_serial_number_two)

    assert mock_gpio_state["state"] == mock_relay_open
    return_value = await temperature_relay_bridge.handle_temperature(mock_temperature_event,
                                                                     None,
                                                                     mock_last_received_time_nanoseconds)

    assert return_value is None
    assert mock_gpio_state["state"] == mock_relay_open


@pytest.mark.asyncio
async def test_monitor_time_elapsed_false(mocker, monkeypatch, event_loop):
    mock_last_received_time = 789
    mock_temp_state = {"last_received_time": mock_last_received_time}
    monkeypatch.setattr(temperature_relay_bridge, 'temp', mock_temp_state)
    mock_relay_closed = 1
    monkeypatch.setattr(temperature_relay_bridge, 'RELAY_CLOSED', mock_relay_closed)
    mock_relay_open = 0
    monkeypatch.setattr(temperature_relay_bridge, 'RELAY_OPEN', mock_relay_open)
    mock_gpio_state = {"state": mock_relay_closed}
    monkeypatch.setattr(temperature_relay_bridge, 'gpio', mock_gpio_state)

    mocker.patch.object(temperature_relay_bridge.itertools, 'repeat', return_value=range(1))
    mocker.patch.object(temperature_relay_bridge.time, 'time', return_value=mock_last_received_time)
    mocker.patch.object(temperature_relay_bridge, 'timer', return_value=0)
    mocker.patch.object(temperature_relay_bridge.asyncio, 'sleep', new_callable=AsyncMock)

    assert mock_gpio_state["state"] == mock_relay_closed
    await temperature_relay_bridge.monitor_time_elapsed(loop=event_loop)

    assert mock_gpio_state["state"] == mock_relay_closed


@pytest.mark.asyncio
async def test_monitor_time_elapsed_true(mocker, monkeypatch, event_loop):
    mock_last_received_time = 789
    mock_temp_state = {"last_received_time": mock_last_received_time}
    monkeypatch.setattr(temperature_relay_bridge, 'temp', mock_temp_state)
    mock_relay_closed = 1
    monkeypatch.setattr(temperature_relay_bridge, 'RELAY_CLOSED', mock_relay_closed)
    mock_relay_open = 0
    monkeypatch.setattr(temperature_relay_bridge, 'RELAY_OPEN', mock_relay_open)
    mock_gpio_state = {"state": mock_relay_closed}
    monkeypatch.setattr(temperature_relay_bridge, 'gpio', mock_gpio_state)

    mocker.patch.object(temperature_relay_bridge.itertools, 'repeat', return_value=range(1))
    mocker.patch.object(temperature_relay_bridge.time, 'time', return_value=mock_last_received_time + 151)
    mocker.patch.object(temperature_relay_bridge, 'timer', return_value=0)
    mocker.patch.object(temperature_relay_bridge.asyncio, 'sleep', new_callable=AsyncMock)

    assert mock_gpio_state["state"] == mock_relay_closed
    await temperature_relay_bridge.monitor_time_elapsed(loop=event_loop)

    assert mock_gpio_state["state"] == mock_relay_open


@pytest.mark.asyncio
async def test_run(mocker, event_loop):
    dts_register_pathway_stub = mocker.stub(name='dts_register_pathway_stub')
    dts_handle_type_stub = mocker.stub(name='dts_handle_type_stub')

    class DtsStub:
        def __init__(self):
            pass

        async def register_pathway(self, _):
            dts_register_pathway_stub(_)

        async def handle_type(self, event, data_handlers=None):
            dts_handle_type_stub(event, data_handlers=data_handlers)

    dts = DtsStub()
    mocker.patch.object(temperature_relay_bridge, 'monitor_time_elapsed', new_callable=AsyncMock)

    register_task, monitor_task, handle_type_task = temperature_relay_bridge.run(dts=dts, loop=event_loop)
    await register_task
    await monitor_task
    await handle_type_task

    dts_register_pathway_stub.assert_called_once_with(temperature_relay_bridge.TemperatureEvent)
    temperature_relay_bridge.monitor_time_elapsed.assert_called_once()
    dts_handle_type_stub.assert_called_once()


@pytest.mark.asyncio
async def test_integration_dts(mocker, monkeypatch, event_loop):
    mock_serial_number = "foo"
    mock_serial_number_bytes = bytes(mock_serial_number, "UTF-8")
    mock_temperature_low = b'10.0'
    mock_temperature_high = b'20.0'
    mock_target_temp = 15.0
    monkeypatch.setattr(temperature_relay_bridge, 'TARGET_TEMP', mock_target_temp)
    mock_relay_closed = 1
    monkeypatch.setattr(temperature_relay_bridge, 'RELAY_CLOSED', mock_relay_closed)
    mock_relay_open = 0
    monkeypatch.setattr(temperature_relay_bridge, 'RELAY_OPEN', mock_relay_open)
    mock_gpio_state = {"state": mock_relay_closed}
    monkeypatch.setattr(temperature_relay_bridge, 'gpio', mock_gpio_state)

    mocker.patch.object(temperature_relay_bridge, 'monitor_time_elapsed', new_callable=AsyncMock)
    mocker.patch.object(temperature_relay_bridge.itertools, 'repeat', return_value=range(1))
    mocker.patch.object(temperature_relay_bridge.time, 'time', return_value=0)
    mocker.patch.object(temperature_relay_bridge, 'timer', return_value=0)

    register_task, monitor_task, handle_type_task = temperature_relay_bridge.run(loop=event_loop)
    await register_task
    await monitor_task
    await handle_type_task
    await asyncio.sleep(0.1, loop=event_loop)

    mock_temperature_event_high = temperature_relay_bridge.TemperatureEvent(
        temperature=mock_temperature_high,
        timestamp=900e9,
        model=b'',
        serial_number=mock_serial_number_bytes)

    mock_temperature_event_low = temperature_relay_bridge.TemperatureEvent(
        temperature=mock_temperature_low,
        timestamp=901e9,
        model=b'',
        serial_number=mock_serial_number_bytes)

    with DomainTypeSystem(loop=event_loop) as dts_send:
        await asyncio.sleep(0.1, loop=event_loop)
        await dts_send.register_pathway(temperature_relay_bridge.TemperatureEvent)

        assert mock_gpio_state["state"] == mock_relay_closed
        await dts_send.send_struct(mock_temperature_event_high)
        await asyncio.sleep(0.1, loop=event_loop)
        assert mock_gpio_state["state"] == mock_relay_open

        await dts_send.send_struct(mock_temperature_event_low)
        await asyncio.sleep(0.1, loop=event_loop)
        assert mock_gpio_state["state"] == mock_relay_closed
