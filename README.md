# temperature_relay_bridge
This project intercepts temperature data from the [Decentralized Type System (DTS)](https://gitlab.com/agates/domain-type-system),
averages data from the latest data from each serial number and controls a GPIO pin
(which on its own is typically hooked up to a relay controlling a heat source).