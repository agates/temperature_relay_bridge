import asyncio
import itertools
import logging
import signal
import time
from timeit import default_timer as timer

import capnpy
from domaintypesystem import DomainTypeSystem

try:
    import RPi.GPIO as GPIO
except RuntimeError as e:
    class RPiGPIOStub:
        BCM = None
        HIGH = True
        LOW = False
        OUT = None
        mode = None
        direction = dict()
        state = dict()
        warnings = None

        def __init__(self):
            pass

        @classmethod
        def output(cls, pin, state):
            cls.state[pin] = state

        @classmethod
        def setmode(cls, mode):
            cls.mode = mode

        @classmethod
        def setwarnings(cls, warnings):
            cls.warnings = warnings

        @classmethod
        def setup(cls, pin, direction, initial=None):
            cls.direction[pin] = direction
            cls.state[pin] = initial


    GPIO = RPiGPIOStub()

TemperatureEvent = capnpy.load_schema(
    'src.temperature_relay_bridge.schema.temperature_event', pyx=False).TemperatureEvent

# Target temperature in celsius
TARGET_TEMP = 36
# TARGET_TEMP = 0
LOWER_THRESHOLD = .3

GPIO_PIN = 17

latest_temperature_events = dict()
latest_temperature_events_lock = asyncio.Lock()

RELAY_OPEN = GPIO.HIGH
RELAY_CLOSED = GPIO.LOW

# initial state set OFF/open (HIGH)
gpio = {'state': RELAY_OPEN}
gpio_lock = asyncio.Lock()

temp = {'last_received_time': 0}


async def handle_temperature(temperature_event, address, received_timestamp_nanoseconds):
    async with latest_temperature_events_lock:
        if temperature_event.serial_number in latest_temperature_events \
                and temperature_event.timestamp <= latest_temperature_events[temperature_event.serial_number].timestamp:
            # Got old temperature data, ignore
            return

        latest_temperature_events[temperature_event.serial_number] = temperature_event
        all_temperatures = [float(event.temperature)
                            for event in latest_temperature_events.values()]
        average_temperature = sum(all_temperatures) / len(all_temperatures)

        temp['last_received_time'] = round(received_timestamp_nanoseconds / 1e9)

    logging.info("Average temperature: {0}°C".format(average_temperature))

    async with gpio_lock:
        if gpio['state'] == RELAY_CLOSED:
            # Currently off
            logging.debug("RELAY_CLOSED")
            if average_temperature >= TARGET_TEMP:
                GPIO.output(GPIO_PIN, RELAY_OPEN)
                gpio['state'] = RELAY_OPEN
                logging.info("Average temperature reached target temp {0}°C, opening relay"
                             .format(TARGET_TEMP))
            else:
                logging.info("Average temperature below target temp {0}°C, keeping relay closed"
                             .format(TARGET_TEMP))
        elif gpio['state'] == RELAY_OPEN:
            # Currently on
            logging.debug("RELAY_OPEN")
            if average_temperature <= TARGET_TEMP - LOWER_THRESHOLD:
                GPIO.output(GPIO_PIN, RELAY_CLOSED)
                gpio['state'] = RELAY_CLOSED
                logging.info("Average temperature reached target temp threshold {0}°C, closing relay"
                             .format(TARGET_TEMP - LOWER_THRESHOLD))
            else:
                logging.info("Average temperature above target temp threshold {0}°C, keeping relay open"
                             .format(TARGET_TEMP - LOWER_THRESHOLD))


async def monitor_time_elapsed(loop=None):
    if loop is None:  # pragma: no cover
        loop = asyncio.get_event_loop()

    await asyncio.sleep(60, loop=loop)

    for _ in itertools.repeat(None):
        start_time = timer()
        async with latest_temperature_events_lock:
            last_received_time = temp['last_received_time']

        if time.time() - last_received_time > 150:
            logging.info("No temperature received for 2.5 minutes, closing relay")
            async with gpio_lock:
                GPIO.output(GPIO_PIN, RELAY_OPEN)
                gpio['state'] = RELAY_OPEN

        await asyncio.sleep(60 - (timer() - start_time), loop=loop)


def run(dts=None, loop=None):
    logging.basicConfig(level=logging.DEBUG)

    if not loop:  # pragma: no cover
        loop = asyncio.new_event_loop()

    if not dts:  # pragma: no cover
        dts = DomainTypeSystem(loop=loop)

    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)

    # BCM gpio pin 17
    GPIO.setup(GPIO_PIN, GPIO.OUT, initial=gpio['state'])

    register_coro = dts.register_pathway(TemperatureEvent)
    monitor_coro = monitor_time_elapsed(loop=loop)
    handle_type_coro = dts.handle_type(TemperatureEvent,
                                       data_handlers=(
                                           handle_temperature,
                                       )
                                       )

    if not loop.is_running():  # pragma: no cover
        loop.run_until_complete(register_coro)
        loop.create_task(monitor_coro)
        loop.create_task(handle_type_coro)
        try:
            signal.signal(signal.SIGINT, signal.default_int_handler)
            loop.run_forever()
        except KeyboardInterrupt:
            pass
        finally:
            # Turn the relay off before shutting down
            GPIO.output(GPIO_PIN, RELAY_OPEN)
            loop.close()
    else:
        return loop.create_task(register_coro), loop.create_task(monitor_coro), loop.create_task(handle_type_coro)
